'use strict';

const Gatherer = require('lighthouse').Gatherer;

class TimeToUrl extends Gatherer {
    afterPass(options) {
        const driver = options.driver;

        return driver.evaluateAsync('window.urlLoadTime')
            .then(urlLoadTime => {
                if (!urlLoadTime) {

                    throw new Error('Unable to find url load metrics in page');
                }
                return urlLoadTime;
            });
    }
}

module.exports = TimeToUrl;