'use strict';

const Audit = require('lighthouse').Audit;

const MAX_URL_TIME = 3000;

class LoadUrlAudit extends Audit {
    static get meta() {
        return {
            category: 'MyPerformance',
            name: 'url-audit',
            description: 'URL info initialized and ready',
            failureDescription: 'RATP URL slow',
            helpText: 'Used to measure time from request to reponse to RATP URL.',

            requiredArtifacts: ['TimeToUrl']
        };
    }

    static audit(artifacts) {
        const loadedTime = artifacts.TimeToUrl;

        const belowThreshold = loadedTime <= MAX_URL_TIME;

        return {
            rawValue: loadedTime,
            score: belowThreshold
        };
    }
}

module.exports = LoadUrlAudit;